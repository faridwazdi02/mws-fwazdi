/************************************
* Authors : Farid Wazdi
* Email   : faridwazdi02@gmail.com
* Google MWS - 2018
************************************/

const url = 'https://mws-fwazdi.firebaseapp.com/taman.json' ;

var result = fetch(url, {headers : {'Content-Type':'aplication/json'}})
            .then( response => response.json())
            .then(addToMap);

function addToMap(data) {
    let mymap = L.map('mapid').setView([-6.580128,106.802815], 13.5);
    const gc = document.getElementById("desImage") ;
    const rt = document.getElementById("reviewText") ;
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', 
        { 
            maxZoom: 20,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZndhemRpMDIiLCJhIjoiY2ptYXJtY2Y0MDF3NzNsbzBqZDUyaGpiMyJ9.Cca1EYLw64pSwjUrypgfiQ'
        }).addTo(mymap);

    for (let i = 0; i < data.length; i++) {
    let mrk = L.marker(data[i].coord).addTo(mymap).bindPopup(`<strong>${data[i].name.toString()}</strong>`).on('click', () => {
            gc.innerHTML = '' ;
            rt.innerHTML = '' ;
            gc.innerHTML = '<img class="place-image" src="'+data[i].image+'">' ;
            rt.innerHTML = `<strong>${data[i].name.toString()}</strong><br/><small>${data[i].address.toString()}<small>` ;
        })

    }
}

