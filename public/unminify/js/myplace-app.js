var place = { coord :[-6.601246, 106.805150],
              title : "<b>Tugu Kujang</b>",
              image : './images/taman-heulang.jpg'
            } ;
var mymap = L.map('mapid').setView([-6.601246, 106.805150], 13.5);

var gc = document.getElementById("desImage") ;

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', 
    { attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZndhemRpMDIiLCJhIjoiY2ptYXJtY2Y0MDF3NzNsbzBqZDUyaGpiMyJ9.Cca1EYLw64pSwjUrypgfiQ'
    }).addTo(mymap);

let mrk = L.marker(place.coord).addTo(mymap).bindPopup(place.title).openPopup();
console.log(place.title);
