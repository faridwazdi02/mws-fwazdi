/************************************
* Authors : Farid Wazdi
* Email   : faridwazdi02@gmail.com
* Google MWS - 2018
************************************/

const vt = document.querySelector("#Add");
const vk = document.querySelector("#Min");
const vkl = document.querySelector("#Multi");
const vb = document.querySelector("#Divd");


vt.addEventListener('click', function(){
    let angkanya = document.querySelectorAll("input");
    let angka1 = parseInt(angkanya[0].value);
    let angka2 = parseInt(angkanya[1].value);
    let hasil = angka1 + angka2 ;
    document.getElementById("hasil").value = hasil.toString();
    fx(this);
});

vk.addEventListener('click', function(){
    let angkanya = document.querySelectorAll("input");
    let angka1 = parseInt(angkanya[0].value);
    let angka2 = parseInt(angkanya[1].value);
    let hasil = angka1 - angka2 ;
    document.getElementById("hasil").value = hasil.toString();
    fx(this);
});

vkl.addEventListener('click', function(){
    let angkanya = document.querySelectorAll("input");
    let angka1 = parseInt(angkanya[0].value);
    let angka2 = parseInt(angkanya[1].value);
    let hasil = angka1 * angka2 ;
    document.getElementById("hasil").value = hasil.toString();
    fx(this);
});

vb.addEventListener('click', function(){
    let angkanya = document.querySelectorAll("input");
    let angka1 = parseFloat(angkanya[0].value);
    let angka2 = parseFloat(angkanya[1].value);
    let hasil = 0;
    if(angka2 !== 0 ){	
    	hasil = angka1 / angka2 ;
    	fx(this);
    }else{
    	alert("the second operand can't be 'zero'") ;
    }
    document.getElementById("hasil").value = hasil.toString();
});

function fx(ele){
	let slist = document.querySelectorAll('ul.switch li.switch-item');
	for(item of slist){
		item.className = 'switch-item' ;
	}
	ele.parentElement.className = 'switch-item active' ;
}


