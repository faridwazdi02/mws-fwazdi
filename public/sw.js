let staticCacheName = 'mws-frd-v2';

self.addEventListener('install', function(event) {
    console.log('Installing...');

    let urlsToCaches = [
    	  '/',
        'css/mymapbox.css',
        'css/mystyle.css',
        'js/add2numbers.js',
        'js/my-app.js',
        'js/main.js',
        'js/mymapbox-app.js',
        'images/avatar.png',
        'images/icon.png',
        'images/tugu-kujang.jpg',
        'images/taman-corat-coret.jpg',
        'images/taman-heulang.jpg',
        'images/taman-kencana.jpg',
        'images/taman-peranginan.jpg',
        'https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh50XSwiPGQ.woff2',
        'https://fonts.gstatic.com/s/lato/v14/S6uyw4BMUTPHjx4wXg.woff2',
        'https://fonts.gstatic.com/s/lato/v14/S6uyw4BMUTPHjxAwXjeu.woff2',
        'https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh6UVSwiPGQ.woff2',
        'https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh7USSwiPGQ.woff2',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'
        ];

    event.waitUntil(
        caches.open(staticCacheName).then(function(cache) {
            return cache.addAll(urlsToCaches);
        })
    );
});

self.addEventListener('activate', function(event) {
    console.log('Activating Event : ', event);

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.filter(function(cacheName) {
                    return cacheName.startsWith('mws-frd-') && cacheName != staticCacheName;
                }).map(function(cacheName) {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    console.log('Fetching Event : ', event);

    event.respondWith(
        caches.match(event.request).then(function(response){
    		console.log('Fetching Request : ', event.request);
            console.log('Fetching Response : ',response);

            return response || fetch(event.request).then(function(response) {

            	if(response.status == 404){
            		return new Response('Sorry, Page Not Found');
            	}

                return caches.open(staticCacheName).then(function(cache) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            }).catch(function(){
        		return new Response('You running on offline mode');
            })
        })
    );
});
